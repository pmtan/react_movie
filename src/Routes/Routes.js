import Home from "../Pages/Home";
import Login from "../Pages/Login";
import MovieDetail from "../Pages/MovieDetail/MovieDetail";
import SeatSelection from "../Pages/SeatSelection/SeatSelection";

export const routes = [
  {
    path: "/login",
    component: <Login />,
  },
  {
    path: "/",
    component: <Home />,
  },
  {
    path: "/detail/:id",
    element: <MovieDetail />,
  },
  {
    path: "/chitietphongve/:id",
    element: <SeatSelection />,
  },
];
