import React from "react";
import HomeBannerCarousel from "../../Components/HomeBannerCarousel/HomeBannerCarousel";
import MoviesCarousel from "../../Components/MoviesCarousel/MoviesCarousel";
import MovieSearchBar from "../../Components/MovieSearchBar/MovieSearchBar";
import NavBar from "../../Components/NavBar/NavBar";
import PageFooter from "../../Components/PageFooter/PageFooter";
import TheaterInfo from "../../Components/TheaterInfo/TheaterInfo";

function Home() {
  return (
    <div className="bg-black">
      <NavBar />
      <HomeBannerCarousel />
      <MoviesCarousel />
      <MovieSearchBar />
      <TheaterInfo />
      <PageFooter />
    </div>
  );
}

export default Home;
