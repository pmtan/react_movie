import React from "react";
import { useEffect, useState } from "react";
import NavBar from "../../Components/NavBar/NavBar";
import { userService } from "../../Services/user.service";
import PageFooter from "../../Components/PageFooter/PageFooter";
import { Button, Form, Input, message } from "antd";
import "./UserInfo.css";
import moment from "moment";

export default function UserInfo() {
  const onFinish = async (values) => {
    console.log("Success:", values);
    try {
      let res = await userService.update(values);
      console.log(res.data);
      message.success(res.data.message);
    } catch (err) {
      console.log(err);
    }
  };
  const onFinishFailed = (errorInfo) => {
    console.log("Failed:", errorInfo);
  };
  const [thongTinDatVe, setThongTinDatVe] = useState([]);
  const [accountInfo, setAccountInfo] = useState({});
  const fetchUserData = async () => {
    try {
      let res = await userService.getAccountInfo();
      // console.log(res.data.content);
      setThongTinDatVe(res.data.content.thongTinDatVe);
      setAccountInfo(res.data.content);
    } catch (err) {
      console.log(err);
    }
  };

  const defaultVales = {
    taiKhoan: accountInfo.taiKhoan,
    matKhau: accountInfo.matKhau,
    hoTen: accountInfo.hoTen,
    email: accountInfo.email,
    soDt: accountInfo.soDT,
    maNhom: accountInfo.maNhom,
    maLoaiNguoiDung: accountInfo.maLoaiNguoiDung,
  };

  const [form] = Form.useForm();

  const renderForm = () => {
    return (
      <div className="container mx-auto">
        <div className="flex flex-col bg-slate-50 lg:w-2/3 mx-auto mt-10">
          <Form
            form={form}
            layout="inline"
            className="grid grid-cols-2 gap-2 w-full p-3 mx-auto rounded"
            labelCol={{
              span: 10,
            }}
            wrapperCol={{
              span: 14,
            }}
            onFinish={onFinish}
            onFinishFailed={onFinishFailed}
            autoComplete="off"
            initialValues={defaultVales}
          >
            <Form.Item label="Tài Khoản" name="taiKhoan">
              <Input />
            </Form.Item>

            <Form.Item label="Mật Khẩu" name="matKhau">
              <Input.Password />
            </Form.Item>

            <Form.Item label="Họ Tên" name="hoTen">
              <Input />
            </Form.Item>

            <Form.Item label="Email" name="email">
              <Input />
            </Form.Item>

            <Form.Item label="Số Điện Thoại" name="soDt">
              <Input />
            </Form.Item>

            <Form.Item className="hidden" label="Mã Nhóm" name="maNhom">
              <Input />
            </Form.Item>

            <Form.Item
              className="hidden"
              label="Loại Người Dùng"
              name="maLoaiNguoiDung"
            >
              <Input />
            </Form.Item>

            <Form.Item>
              <div className="flex justify-end pt-3">
                <Button type="primary" htmlType="submit" danger>
                  Cập Nhât
                </Button>
              </div>
            </Form.Item>
          </Form>
        </div>
        <div className="py-5 text-sm font-medium">
          <h1>LỊCH SỬ ĐẶT VÉ</h1>
          <table id="lich-su-dat-ve" className="table-auto w-full p-10">
            <thead>
              <tr className="text-red-600 bg-slate-50 border">
                <th className="text-sm">Mã Vé</th>
                <th>Tên Phim</th>
                <th>Ngày Đặt</th>
                <th>Ghế</th>
                <th>Rạp</th>
                <th>Tên Rạp</th>
              </tr>
            </thead>
            <tbody>{renderTableBody()}</tbody>
          </table>
        </div>
      </div>
    );
  };
  const renderTableBody = () => {
    return thongTinDatVe.map((ve) => {
      return (
        <tr
          className="border font-medium hover:bg-slate-100 hover:cursor-pointer  transition ease-in-out"
          key={ve.maVe}
        >
          <td className="py-3">{ve.maVe}</td>
          <td>{ve.tenPhim}</td>
          <td>{moment(ve.ngayDat).format(" h:mm:ss a, DD/MM/YYYY")}</td>
          <td>
            {ve.danhSachGhe.map((ghe) => {
              return ghe.tenGhe + " ";
            })}
          </td>
          <td>{ve.danhSachGhe[0].tenCumRap}</td>
          <td>{ve.danhSachGhe[0].tenHeThongRap}</td>
        </tr>
      );
    });
  };
  useEffect(() => {
    fetchUserData();
  }, []);
  useEffect(() => form.resetFields(), [defaultVales]);
  return (
    <div className="background">
      <div className="h-full">
        <NavBar />
        {renderForm()}
        <PageFooter />
      </div>
    </div>
  );
}
