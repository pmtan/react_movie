import React, { useEffect } from "react";
import { Button, Form, Input, message } from "antd";
import { useNavigate } from "react-router-dom";
import { localService } from "../../Services/local.service";
import { setUserInfoStore } from "../../Redux/UserSlice";
import { userService } from "../../Services/user.service";
import { useDispatch } from "react-redux";
import Lottie from "lottie-react";
import bg_login from "../../Asset/bg_login.json";

function Login() {
  const dispatch = useDispatch();
  const navigate = useNavigate();
  useEffect(() => {
    let userData = localService.get();
    if (userData) {
      navigate("/");
    }
  }, [navigate]);

  const onFinish = async (values) => {
    try {
      const res = await userService.postLogin(values);
      message.success(res.data.message);
      localService.set(res.data.content);
      dispatch(setUserInfoStore(res.data.content));
      navigate("/");
    } catch (err) {
      // console.log(err);
      message.error(err.response.data.content);
    }
  };
  const onFinishFailed = (errorInfo) => {
    console.log("Failed:", errorInfo);
  };
  return (
    <div className="background">
      <div className="h-screen flex items-center justify-center">
        <div className="flex items-center p-3 mx-auto w-11/12 lg:w-1/2 bg-slate-50 rounded">
          <div className="w-1/3 lg:scale-75">
            <Lottie animationData={bg_login} />
          </div>
          <div className="w-2/3 px-3 lg:px-10">
            <Form
              layout="verticle"
              name=""
              labelCol={{
                span: 0,
              }}
              wrapperCol={{
                span: 24,
              }}
              initialValues={{
                remember: true,
              }}
              onFinish={onFinish}
              onFinishFailed={onFinishFailed}
              autoComplete="off"
            >
              <Form.Item
                label="Tài Khoản"
                name="taiKhoan"
                rules={[
                  {
                    required: true,
                    message: "Vui lòng điền tên tài khoản!",
                  },
                ]}
              >
                <Input />
              </Form.Item>

              <Form.Item
                label="Mật Khẩu"
                name="matKhau"
                rules={[
                  {
                    required: true,
                    message: "Vui lòng nhập mật khẩu!",
                  },
                ]}
              >
                <Input.Password />
              </Form.Item>

              <Form.Item
                wrapperCol={{
                  offset: 0,
                  span: 24,
                }}
              >
                <Button
                  className="bg-slate-600 text-white rounded"
                  type="primary"
                  htmlType="submit"
                >
                  Submit
                </Button>
              </Form.Item>
            </Form>
          </div>
        </div>
      </div>
    </div>
  );
}

export default Login;
