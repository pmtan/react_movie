import { Button, Select, Form, Input, message } from "antd";
import React from "react";
import { useNavigate } from "react-router-dom";
import { userService } from "../../Services/user.service";
import "./Register.css";

export default function Register() {
  const navigate = useNavigate();
  const onFinish = (values) => {
    console.log("Success:", values);
    register(values);
  };

  const onFinishFailed = (errorInfo) => {
    console.log("Failed:", errorInfo);
  };
  const register = async (values) => {
    try {
      let res = await userService.register(values);
      console.log(res.data);
      message.success(res.data.message);
      let status = res.data.statusCode;
      if (status === 200) {
        navigate("/login");
      }
      return;
    } catch (err) {
      console.log(err.response.data);
      message.error(err.response.data.content);
    }
  };
  return (
    <div className="background flex justify-around">
      <Form
        id="register-form"
        className="w-full lg:w-2/3 mx-auto p-10 rounded"
        autoComplete="off"
        labelCol={{ span: 8 }}
        wrapperCol={{ span: 16 }}
        onFinish={onFinish}
        onFinishFailed={onFinishFailed}
      >
        <h1 className="text-3xl font-medium mb-5">Thông Tin Đăng Ký</h1>
        <Form.Item
          label="Tài Khoản"
          name="taiKhoan"
          rules={[
            { required: true, message: "Vui lòng nhập tài khoản!" },
            {
              whitespace: "true",
            },
          ]}
          hasFeedback
        >
          <Input />
        </Form.Item>

        <Form.Item
          label="Mật Khẩu"
          name="matKhau"
          rules={[{ required: true, message: "Vui lòng nhập mật khẩu!" }]}
          hasFeedback
        >
          <Input.Password />
        </Form.Item>

        <Form.Item
          label="Xác Nhận Mật Khẩu"
          name="xacNhanMatKhau"
          dependencies={["matKhau"]}
          rules={[
            { required: true, message: "Vui lòng nhập mật khẩu để xác minh!" },
            ({ getFieldValue }) => ({
              validator(_, value) {
                if (!value || getFieldValue("matKhau") === value) {
                  return Promise.resolve();
                }
                return Promise.reject("Mật khẩu không trùng khớp.");
              },
            }),
          ]}
          hasFeedback
        >
          <Input.Password />
        </Form.Item>

        <Form.Item
          label="Email"
          name="email"
          rules={[
            { required: true, message: "Vui lòng nhâp email!" },
            { type: "email", message: "Email không hợp lệ." },
          ]}
          hasFeedback
        >
          <Input />
        </Form.Item>

        <Form.Item
          label="Họ Tên"
          name="hoTen"
          rules={[{ required: true, message: "Vui lòng nhập họ tên!" }]}
        >
          <Input />
        </Form.Item>

        <Form.Item
          label="Số Điện Thoại"
          name="soDt"
          rules={[
            { required: true, message: "Vui lòng nhập số điện thoại!" },
            {
              pattern:
                /^(0?)(3[2-9]|5[6|8|9]|7[0|6-9]|8[0-6|8|9]|9[0-4|6-9])[0-9]{7}$/,
            },
          ]}
        >
          <Input />
        </Form.Item>

        <Form.Item
          label="Mã Nhóm"
          name="maNhom"
          rules={[{ required: true, message: "Vui lòng chọn mã nhóm!" }]}
        >
          <Select>
            <Select.Option value="GP00">GP00</Select.Option>
            <Select.Option value="GP01">GP01</Select.Option>
            <Select.Option value="GP02">GP02</Select.Option>
            <Select.Option value="GP03">GP03</Select.Option>
            <Select.Option value="GP04">GP04</Select.Option>
            <Select.Option value="GP05">GP05</Select.Option>
          </Select>
        </Form.Item>

        <Form.Item className="text-right">
          <Button
            type="default"
            className="rounded"
            htmlType="submit"
            size="large"
            danger
          >
            Đăng Ký
          </Button>
          <Button
            onClick={() => navigate("/")}
            size="large"
            danger
            className="ml-3 rounded"
          >
            Quay Lại
          </Button>
        </Form.Item>
      </Form>
    </div>
  );
}
