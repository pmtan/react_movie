import React, { useEffect, useState } from "react";
import { ticketService } from "../../Services/ticket.service";
import { useNavigate, useParams } from "react-router-dom";
import NavBar from "../../Components/NavBar/NavBar";
import { Button, message, Modal, Result } from "antd";
import "./seatSelection.css";
import PageFooter from "../../Components/PageFooter/PageFooter";
import axios from "axios";
import { BASE_URL, TOKEN_CYBERSOFT } from "../../Services/url.config";
import { localService } from "../../Services/local.service";

const SeatSelection = () => {
  let { id } = useParams();
  let navigate = useNavigate();
  const [danhSachVe, setDanhSachVe] = useState([]);
  // console.log(danhSachVe);
  const [thongTinPhim, setThongTinPhim] = useState({});
  const renderTableBody = () => {
    return danhSachVe.map((ve) => {
      return (
        <tr key={ve.tenGhe}>
          <td className="border border-slate-600">{ve.tenGhe}</td>
          <td className="border border-slate-600">
            {ve.loaiGhe === "Thuong" ? "Thường" : "VIP"}
          </td>
          <td className="border border-slate-600">{ve.giaVe} VND</td>
        </tr>
      );
    });
  };
  let total = danhSachVe.reduce((total, ve) => {
    return total + ve.giaVe;
  }, 0);
  const renderHoaDon = () => {
    return (
      <div className="flex flex-col justify-center items-center w-full hoa-don p-3 lg:p-5 lg:mx-5">
        <img
          src={thongTinPhim.hinhAnh}
          className="w-20 lg:w-32"
          alt="hinh-phim"
        />
        <div className="flex flex-col items-center">
          <h1 className="text-xl lg:text-2xl font-bold">
            {thongTinPhim.tenPhim}
          </h1>
          <p className="text-md lg:text-xl font-bold text-red-500">
            {thongTinPhim.tenCumRap}
          </p>
          <p className="text-md font-medium">Địa Chỉ: {thongTinPhim.diaChi}</p>
          <div className="bg-green-400 rounded p-1 text-md lg:text-lg w-2/3">
            <div className="px-3 ">Giờ Chiếu: {thongTinPhim.gioChieu}</div>
            <div>Ngày Chiếu: {thongTinPhim.ngayChieu}</div>
            <div className="flex justify-center">
              <span className="bg-slate-50 rounded w-1/4">
                {thongTinPhim.tenRap}
              </span>
            </div>
          </div>

          <div className="py-5">
            <table className="table-fixed w-full border border-slate-600 rounded">
              <thead className="border border-slate-600">
                <tr className="border border-slate-600">
                  <th className="border border-slate-600">Số Ghế</th>
                  <th className="border border-slate-600">Loại Ghế</th>
                  <th className="border border-slate-600">Giá Tiền</th>
                </tr>
              </thead>
              <tbody>{renderTableBody()}</tbody>
              <tfoot>
                <tr>
                  <td className="text-md  font-medium">
                    <span>Tổng số ghế:</span>
                    <span className="bg-red-600 px-1 mx-1 rounded text-white">
                      {danhSachVe.length}
                    </span>
                  </td>
                  <td className="text-md font-medium text-right">Tổng Tiền</td>
                  <td>
                    <span className="text-md font-medium bg-red-600 px-2 rounded text-white">
                      {new Intl.NumberFormat().format(total)} VND
                    </span>
                  </td>
                </tr>
              </tfoot>
            </table>
          </div>
          <Button onClick={xacNhanDatVe} type="primary" danger>
            Xác Nhận Đặt Vé
          </Button>
        </div>
      </div>
    );
  };

  const xacNhanDatVe = async () => {
    const dataDatVe = {
      maLichChieu: parseInt(id),
      danhSachVe: danhSachVe,
    };
    console.log(dataDatVe);
    axios({
      url: `${BASE_URL}/api/QuanLyDatVe/DatVe`,
      method: "POST",
      headers: {
        TokenCybersoft: TOKEN_CYBERSOFT,
        Authorization: "Bearer " + localService.get()?.accessToken,
      },
      data: dataDatVe,
    })
      .then((res) => {
        console.log(res.data.content);
        message.success(res.data.content);
        showModal();
      })
      .catch((err) => {
        console.log(err);
        message.error("Đã xảy ra lỗi, vui lòng thử lại sau.");
      });
    // try {
    //   let res = await ticketService.xacNhanDatVe(dataDatVe);
    //   message.success(res.data.content);
    //   showModal();
    //   console.log(res.data);
    // } catch (err) {
    //   message.error("Đã xảy ra lỗi, vui lòng thử lại sau.");
    //   console.log(err);
    // }
  };
  const handleChonGhe = (e, ghe) => {
    let cloneDanhSachVe = [...danhSachVe];
    let index = cloneDanhSachVe.findIndex((item) => item.maGhe === ghe.maGhe);
    if (index !== -1) {
      e.target.classList.remove("bg-green-400");
      cloneDanhSachVe.splice(index, 1);
      setDanhSachVe(cloneDanhSachVe);
      return;
    } else {
      e.target.classList.add("bg-green-400");
      setDanhSachVe([...danhSachVe, ghe]);
      return;
    }
  };
  const [danhSachGhe, setDanhSachGhe] = useState([]);
  const renderDanhSachGhe = () => {
    return (
      <div className=" grid grid-cols-16 gap-1 lg:gap-2 pb-3 lg:p-5">
        {danhSachGhe.map((ghe) => {
          if (ghe.daDat === true) {
            return (
              <div key={ghe.maGhe} className="bg-slate-500 rounded">
                X
              </div>
            );
          } else {
            return (
              <div
                onClick={(e) => {
                  handleChonGhe(e, ghe);
                }}
                key={ghe.maGhe}
                className="border border-slate-900 p-1 text-xs center lg:text-sm lg:p-2 rounded hover:bg-green-400 hover:cursor-pointer transition-all "
              >
                {ghe.tenGhe}
              </div>
            );
          }
        })}
      </div>
    );
  };
  const [isModalOpen, setIsModalOpen] = useState(false);

  const showModal = () => {
    setIsModalOpen(true);
  };

  const handleOk = () => {
    setIsModalOpen(false);
  };

  const handleCancel = () => {
    setIsModalOpen(false);
  };

  const fetchMovieSeat = async (maLichChieu) => {
    try {
      let res = await ticketService.getMovieSeatData(maLichChieu);
      // console.log(res.data.content);
      setDanhSachGhe(res.data.content.danhSachGhe);
      setThongTinPhim(res.data.content.thongTinPhim);
    } catch (err) {
      console.log(err);
    }
  };
  useEffect(() => {
    fetchMovieSeat({ MaLichChieu: id });
  }, [id]);

  return (
    <div className="mx-auto background">
      <NavBar />
      <div className="container flex items-start mx-auto m-20 flex-col lg:flex-row">
        <div className="flex flex-col items-center seat-selection-map w-screen lg:w-2/3">
          <div className="bg-slate-900 w-4/5 my-5 lg:mb-10 rounded">
            <h1 className="text-slate-50">MÀN HÌNH</h1>
          </div>
          {renderDanhSachGhe()}
        </div>
        <div className="lg:w-2/5">
          {danhSachVe.length > 0 && renderHoaDon()}
        </div>
      </div>
      <PageFooter />
      <Modal
        title=""
        open={isModalOpen}
        onOk={handleOk}
        onCancel={handleCancel}
        footer={null}
      >
        <Result
          status="success"
          title="Đặt Vé Thành Công!"
          extra={[
            <Button
              key={"go-to-homepage"}
              className="mt-2"
              danger
              onClick={() => navigate("/")}
            >
              Quay lại trang chủ
            </Button>,
            <Button
              key={"go-to-user-info"}
              className="mt-2"
              danger
              onClick={() => navigate("/user-info")}
            >
              Xem lịch sử đặt vé
            </Button>,
          ]}
        />
      </Modal>
    </div>
  );
};

export default SeatSelection;
