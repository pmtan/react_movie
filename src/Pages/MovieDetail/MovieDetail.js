import React, { useEffect, useState } from "react";
import NavBar from "../../Components/NavBar/NavBar";
import { movieService } from "../../Services/movie.service";
import { Button, Modal, Tabs, message, Form, Input } from "antd";
import moment from "moment";
import { useNavigate, useParams } from "react-router-dom";
import ReactPlayer from "react-player";
import "./movie-detail.css";
import { localService } from "../../Services/local.service";
import { useDispatch, useSelector } from "react-redux";
import { userService } from "../../Services/user.service";
import { setMaLichChieu, setUserInfoStore } from "../../Redux/UserSlice";
import PageFooter from "../../Components/PageFooter/PageFooter";

const MovieDetail = () => {
  let { id } = useParams();
  let navigate = useNavigate();
  let dispatch = useDispatch();
  const [movie, setMovie] = useState({});
  const [thongTinRap, setThongTinRap] = useState([]);
  const fetchMovieData = async (params) => {
    try {
      let res = await movieService.getMovieData(params);
      setMovie(res.data.content);
      setThongTinRap(res.data.content.heThongRapChieu);
      // console.log(res.data.content);
    } catch (err) {
      console.log(err);
    }
  };
  const items = [];
  thongTinRap.map((cumRap) => {
    return items.push({
      label: (
        <img
          src={cumRap.logo}
          className="w-8 h-8 lg:w-20 lg:h-20"
          alt="theater-logo"
        />
      ),
      key: cumRap.maHeThongRap,
      children: (
        <div className="h-full overflow-scroll">
          {cumRap.cumRapChieu.map((rap) => {
            return (
              <div key={rap.maCumRap}>
                <div className="flex flex-col w-full bg-slate-900 items-center p-3 mb-3 rounded lg:flex-row">
                  <img
                    src={rap.hinhAnh}
                    className="w-20 h-20 lg:w-32 lg:h-32"
                    alt="theater-img"
                  />
                  <div className="w-full mt-5 lg:m-5 flex flex-col justify-between">
                    <div className="p-3 bg-white rounded">
                      <h1 className="text-md lg:text-lg text-red-500 rounded font-bold">
                        {rap.tenCumRap}
                      </h1>
                      <h1 className="font-medium text-md px-1">
                        <span className="text-slate-900 font-semibold lg:font-bold">
                          {" "}
                          Địa Chỉ:{" "}
                        </span>
                        <span className="text-slate-900 font-medium lg:font-semibold">
                          {rap.diaChi}.
                        </span>
                      </h1>
                    </div>
                    <div className="flex flex-col lg:flex-row">
                      <h1 className="text-white lg:text-lg font-bold my-2 lg:my-5">
                        Chọn Lịch Chiếu:
                      </h1>
                      <div className="flex flex-col lg:grid lg:grid-cols-3 lg:gap-2 lg:p-2 lg:m-1">
                        {rap.lichChieuPhim.map((lichChieu) => {
                          return (
                            <span
                              onClick={() => {
                                let maLich = lichChieu.maLichChieu;
                                handleChonLichChieu(maLich);
                              }}
                              key={lichChieu.maLichChieu}
                              className="bg-green-400 font-semibold rounded p-2 m-1"
                            >
                              {moment(lichChieu.ngayChieuGioChieu).format(
                                "HH:MM - DD/MM/YYYY"
                              )}
                            </span>
                          );
                        })}
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            );
          })}
        </div>
      ),
    });
  });

  const [videoPlayer, setVideoPlayer] = useState(false);
  const showVideoPlayer = () => {
    setVideoPlayer(true);
  };
  const hideVideoPlayer = () => {
    setVideoPlayer(false);
  };

  const [modalDatVe, setModalDatVe] = useState(false);
  const showModalDatVe = () => {
    setModalDatVe(true);
  };
  const hideModalDatVe = () => {
    setModalDatVe(false);
  };

  const [modalLogin, setModalLogin] = useState(false);

  const handleChonLichChieu = (maLC) => {
    dispatch(setMaLichChieu(maLC));
    const userInfo = localService.get();
    if (userInfo !== null) {
      navigate(`/chitietphongve/${maLC}`);
    } else {
      message.warning("Chưa đăng nhập! Đăng nhập để đặt vé.");
      showModalLogin();
    }
  };

  const showModalLogin = () => {
    setModalLogin(true);
  };
  const hideModalLogin = () => {
    setModalLogin(false);
  };
  const maLichChieu = useSelector((state) => state.user.maLichChieu);
  const afterClose = () => {
    const userInfo = localService.get();
    if (userInfo !== null) {
      navigate(`/chitietphongve/${maLichChieu}`);
    } else {
      return;
    }
  };
  const onFinishFailed = (errorInfo) => {
    console.log("Failed:", errorInfo);
  };
  const onFinish = async (values) => {
    // console.log("Success:", values);
    try {
      const res = await userService.postLogin(values);
      message.success("Đăng nhập thành công!");
      localService.set(res.data.content);
      dispatch(setUserInfoStore(res.data.content));
      hideModalLogin();
    } catch (err) {
      // console.log(err);
      message.error(err.response.data.content);
    }
  };

  useEffect(() => {
    fetchMovieData({ MaPhim: id });
  }, [id]);

  return (
    <div className="background">
      <NavBar />
      <div className="container mx-auto lg:my-20">
        <div className="movie-detail flex flex-col justify-around w-full h-full items-center mx-auto mt-10 lg:mt-20 lg:flex lg:flex-row lg:w-4/5">
          <img
            className="w-1/2 pt-5 lg:p-3"
            src={movie.hinhAnh}
            alt="movie-img"
          />
          <div className="w-full lg:w-1/2 p-3 flex flex-col items-center ">
            <h1 className="text-slate-900 font-bold text-3xl py-3 lg:py-5 lg:text-4xl">
              {movie.tenPhim}
            </h1>
            <p className="lg:text-lg text-slate-900">{movie.moTa}</p>
            <div className="flex pt-5">
              <div className="text-lg font-bold text-slate-900">
                Ngày Khởi Chiếu:
              </div>
              <div className="text-lg bg-green-400 rounded text-slate-900 px-3 mx-3">
                <p>
                  {moment(movie.ngayKhoiChieu).format("HH:MM - DD/MM/YYYY")}
                </p>
              </div>
            </div>
            <div className="pt-10 flex justify-between w-2/3 ">
              <Button
                onClick={showVideoPlayer}
                className="rounded"
                type="primary"
                danger
                size="large"
              >
                Xem Trailer
              </Button>
              <Button
                onClick={showModalDatVe}
                className="rounded"
                type="primary"
                danger
                size="large"
              >
                Đặt Vé
              </Button>
            </div>
          </div>
        </div>
      </div>
      <Modal
        open={videoPlayer}
        footer={null}
        onCancel={hideVideoPlayer}
        bodyStyle={{ padding: "0", margin: "auto" }}
        destroyOnClose
      >
        <div className="player-wrapper">
          <ReactPlayer
            className="react-player"
            url={movie.trailer}
            playing={videoPlayer}
            width="100%"
            height="100%"
          />
        </div>
      </Modal>

      <Modal
        width={1200}
        footer={null}
        title="Đặt Vé"
        open={modalDatVe}
        onCancel={hideModalDatVe}
        bodyStyle={{ height: "100%" }}
        destroyOnClose
      >
        <Tabs
          tabPosition="top"
          items={items}
          animated
          centered
          tabBarGutter={30}
        />
      </Modal>

      <Modal
        title="Đăng Nhập"
        open={modalLogin}
        footer={null}
        afterClose={afterClose}
        onCancel={hideModalLogin}
        destroyOnClose
      >
        <Form
          name="basic"
          labelCol={{ span: 8 }}
          wrapperCol={{ span: 16 }}
          initialValues={{ remember: true }}
          onFinish={onFinish}
          onFinishFailed={onFinishFailed}
          autoComplete="off"
        >
          <Form.Item
            label="Tài Khoản"
            name="taiKhoan"
            rules={[{ required: true, message: "Vui lòng điền tài khoản!" }]}
          >
            <Input />
          </Form.Item>

          <Form.Item
            label="Mật Khẩu"
            name="matKhau"
            rules={[{ required: true, message: "Vui lòng nhập mật khẩu!" }]}
          >
            <Input.Password />
          </Form.Item>

          <Form.Item wrapperCol={{ offset: 8, span: 16 }}>
            <Button type="primary" htmlType="submit" danger>
              Đăng Nhập
            </Button>
          </Form.Item>
          <p>
            Bạn chưa có tài khoản?{" "}
            <Button
              type="link"
              onClick={() => {
                setTimeout(() => {
                  navigate("/register");
                }, 1000);
              }}
              className="text-red-500 underline hover:text-green-400 p-0"
            >
              Đăng Ký
            </Button>
          </p>
        </Form>
      </Modal>
      <PageFooter />
    </div>
  );
};

export default MovieDetail;
