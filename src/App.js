import "./App.css";
import { BrowserRouter, Route, Routes } from "react-router-dom";
import Home from "./Pages/HomePage/Home";
import Login from "./Pages/LoginPage/Login";
import MovieDetail from "./Pages/MovieDetail/MovieDetail";
import SeatSelection from "./Pages/SeatSelection/SeatSelection";
import UserInfo from "./Pages/UserInfoPage/UserInfo";
import Register from "./Pages/RegisterPage/Register";
import SecureView from "./HOC/SecureView";
import Spinner from "./Components/Spinner/Spinner";

function App() {
  return (
    <div className="App">
      <BrowserRouter>
        <Spinner />
        <Routes>
          {/* {routes.map(({ path, component }, index) => {
            return <Route key={index} path={path} element={component} />;
          })} */}
          <Route path="/" element={<Home />} />
          <Route path="/login" element={<Login />} />
          <Route path="/detail/:id" element={<MovieDetail />} />
          <Route path="/chitietphongve/:id" element={<SeatSelection />} />
          <Route
            path="/user-info"
            element={<SecureView Component={UserInfo} />}
          />
          <Route path="/register" element={<Register />} />
        </Routes>
      </BrowserRouter>
    </div>
  );
}

export default App;
