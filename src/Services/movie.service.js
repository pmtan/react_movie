import { https } from "./url.config";

export const movieService = {
  getBanner: () => {
    let uri = "/api/QuanLyPhim/LayDanhSachBanner";
    return https.get(uri);
  },
  getMoviesList: (params) => {
    let uri = "/api/QuanLyPhim/LayDanhSachPhimPhanTrang";
    return https.get(uri, { params });
  },
  getMovieData: (params) => {
    let uri = "/api/QuanLyRap/LayThongTinLichChieuPhim";
    return https.get(uri, { params });
  },
  getTheaterInfo: () => {
    let uri = "/api/QuanLyRap/LayThongTinLichChieuHeThongRap";
    return https.get(uri);
  },
};
