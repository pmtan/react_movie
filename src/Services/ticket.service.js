import { https } from "./url.config";

export const ticketService = {
  getMovieSeatData: (params) => {
    let uri = "/api/QuanLyDatVe/LayDanhSachPhongVe";
    return https.get(uri, { params });
  },
  xacNhanDatVe: (data) => {
    let uri = "/api/QuanLyDatVe/DatVe";
    return https.post(uri, data);
  },
};
