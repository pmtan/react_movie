import { https } from "./url.config";

export const userService = {
  postLogin: (loginData) => {
    let uri = "/api/QuanLyNguoiDung/DangNhap";
    return https.post(uri, loginData);
  },
  getAccountInfo: () => {
    let uri = "/api/QuanLyNguoiDung/ThongTinTaiKhoan";
    return https.post(uri);
  },
  register: (values) => {
    let uri = "/api/QuanLyNguoiDung/DangKy";
    return https.post(uri, values);
  },
  update: (data) => {
    let uri = "/api/QuanLyNguoiDung/CapNhatThongTinNguoiDung";
    return https.put(uri, data);
  },
};
