const USER_INFO = "USER_INFO";

export const localService = {
  set: (data) => {
    let jsonData = JSON.stringify(data);
    localStorage.setItem(USER_INFO, jsonData);
  },
  get: () => {
    let data = localStorage.getItem(USER_INFO);
    return JSON.parse(data);
  },
  remove: () => {
    localStorage.removeItem(USER_INFO);
  },
};
