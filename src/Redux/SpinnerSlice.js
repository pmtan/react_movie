import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  isLoading: false,
  count: 0,
};
const SpinnerSlice = createSlice({
  name: "spinnerSlice",
  initialState,
  reducers: {
    batLoading: (state) => {
      state.isLoading = true;
      state.count++;
    },
    tatLoading: (state) => {
      state.count--;
      if (state.count === 0) {
        state.isLoading = false;
      }
    },
  },
});
export const { batLoading, tatLoading } = SpinnerSlice.actions;
export default SpinnerSlice.reducer;
