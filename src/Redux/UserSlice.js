const { createSlice } = require("@reduxjs/toolkit");

const initialState = {
  userInfo: null,
  maLichChieu: null,
};

const UserSlice = createSlice({
  name: "userSlice",
  initialState,
  reducers: {
    setUserInfoStore: (state, action) => {
      state.userInfo = action.payload;
    },
    setMaLichChieu: (state, action) => {
      state.maLichChieu = action.payload;
    },
  },
});
export const { setUserInfoStore, setMaLichChieu } = UserSlice.actions;
export default UserSlice.reducer;
