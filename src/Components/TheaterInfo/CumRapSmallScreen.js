import React, { useState } from "react";
import moment from "moment";
import { Collapse, Form, Input, Button, Modal, message } from "antd";
import { userService } from "../../Services/user.service";
import { localService } from "../../Services/local.service";
import { useNavigate } from "react-router-dom";
import { useDispatch } from "react-redux";
import { setUserInfoStore } from "../../Redux/UserSlice";

const { Panel } = Collapse;

export default function CumRapSmallScreen({ listCumRap }) {
  let userInfo = localService.get();
  const [maLichChieu, setMaLichChieu] = useState(null);
  let navigate = useNavigate();
  let dispatch = useDispatch();
  const [modalLogin, setModalLogin] = useState(false);
  const showModalLogin = () => {
    setModalLogin(true);
  };
  const hideModalLogin = () => {
    setModalLogin(false);
  };
  const onFinishFailed = (errorInfo) => {
    console.log("Failed:", errorInfo);
  };
  const onFinish = async (values) => {
    // console.log("Success:", values);
    try {
      const res = await userService.postLogin(values);
      message.success("Đăng nhập thành công!");
      localService.set(res.data.content);
      dispatch(setUserInfoStore(res.data.content));
      hideModalLogin();
    } catch (err) {
      // console.log(err);
      message.error(err.response.data.content);
    }
  };
  const afterClose = () => {
    if (userInfo !== null) {
      navigate(`/chitietphongve/${maLichChieu}`);
    } else {
      return;
    }
  };
  const handleDangNhap = (idLichChieu) => {
    setMaLichChieu(idLichChieu);
    if (userInfo !== null) {
      navigate(`/chitietphongve/${idLichChieu}`);
    } else {
      message.warning("Chưa đăng nhập! Đăng nhập để đặt vé.");
      showModalLogin();
    }
  };
  return (
    <div>
      <Collapse accordion ghost destroyInactivePanel>
        {listCumRap.map((cumRap) => {
          return (
            <Panel
              header={cumRap.tenCumRap}
              key={cumRap.maCumRap}
              className="bg-slate-300"
            >
              {cumRap.danhSachPhim.map((phim) => {
                return (
                  <Collapse key={phim.maPhim} accordion destroyInactivePanel>
                    <Panel header={phim.tenPhim} className="bg-slate-400">
                      <div className="flex flex-col items-center bg-slate-500 p-3">
                        <img
                          src={phim.hinhAnh}
                          className="w-20 h-20 my-2"
                          alt="movie"
                        />
                        <h1 className="text-slate-50 font-bold">
                          Chọn Lịch Chiếu:
                        </h1>
                        <div className="grid grid-cols-3 gap-2">
                          {phim.lstLichChieuTheoPhim.map((lichChieu) => {
                            return (
                              <span
                                onClick={() => {
                                  let maLich = lichChieu.maLichChieu;
                                  handleDangNhap(maLich);
                                }}
                                key={lichChieu.maLichChieu}
                                className="bg-green-400 rounded p-2 hover:cursor-pointer hover:bg-green-200 transition-all"
                              >
                                {moment(lichChieu.ngayChieuGioChieu).format(
                                  "HH:MM- DD/MM/YYYY"
                                )}
                              </span>
                            );
                          })}
                        </div>
                      </div>
                    </Panel>
                  </Collapse>
                );
              })}
            </Panel>
          );
        })}
      </Collapse>
      <Modal
        destroyOnClose
        title="Đăng Nhập"
        open={modalLogin}
        footer={null}
        afterClose={afterClose}
        onCancel={hideModalLogin}
      >
        <Form
          name="basic"
          labelCol={{ span: 8 }}
          wrapperCol={{ span: 16 }}
          initialValues={{ remember: true }}
          onFinish={onFinish}
          onFinishFailed={onFinishFailed}
          autoComplete="off"
        >
          <Form.Item
            label="Tài Khoản"
            name="taiKhoan"
            rules={[{ required: true, message: "Vui lòng điền tài khoản!" }]}
          >
            <Input />
          </Form.Item>

          <Form.Item
            label="Mật Khẩu"
            name="matKhau"
            rules={[{ required: true, message: "Vui lòng nhập mật khẩu!" }]}
          >
            <Input.Password />
          </Form.Item>

          <Form.Item wrapperCol={{ offset: 8, span: 16 }}>
            <Button type="primary" htmlType="submit" danger>
              Đăng Nhập
            </Button>
          </Form.Item>
          <p>
            Bạn chưa có tài khoản?{" "}
            <Button
              type="link"
              onClick={() => {
                setTimeout(() => {
                  navigate("/register");
                }, 1000);
              }}
              className="text-red-500 underline hover:text-green-400 p-0"
            >
              Đăng Ký
            </Button>
          </p>
        </Form>
      </Modal>
    </div>
  );
}
