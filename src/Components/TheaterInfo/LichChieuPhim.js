import moment from "moment";
import React, { useState } from "react";
import { useNavigate } from "react-router-dom";
import { localService } from "../../Services/local.service";
import { Button, Modal, Form, Input, message } from "antd";
import { userService } from "../../Services/user.service";
import { setMaLichChieu, setUserInfoStore } from "../../Redux/UserSlice";
import { useDispatch, useSelector } from "react-redux";

const LichChieuPhim = ({ danhSachPhim }) => {
  const navigate = useNavigate();
  const dispatch = useDispatch();
  const [modalLogin, setModalLogin] = useState(false);
  const showModalLogin = () => {
    setModalLogin(true);
  };
  const hideModalLogin = () => {
    setModalLogin(false);
  };
  const onFinishFailed = (errorInfo) => {
    console.log("Failed:", errorInfo);
  };
  const onFinish = async (values) => {
    // console.log("Success:", values);
    try {
      const res = await userService.postLogin(values);
      message.success("Đăng nhập thành công!");
      localService.set(res.data.content);
      dispatch(setUserInfoStore(res.data.content));
      hideModalLogin();
    } catch (err) {
      console.log(err);
      message.error(err.response.data.content);
    }
  };
  const maLichChieu = useSelector((state) => state.user.maLichChieu);

  const afterClose = () => {
    let userInfo = localService.get();
    if (userInfo !== null) {
      navigate(`/chitietphongve/${maLichChieu}`);
    } else {
      return;
    }
  };
  const handleChonLichChieu = (maLC) => {
    dispatch(setMaLichChieu(maLC));
    let userInfo = localService.get();
    if (userInfo !== null) {
      navigate(`/chitietphongve/${maLC}`);
    } else {
      message.warning("Chưa đăng nhập! Đăng nhập để đặt vé.");
      showModalLogin();
    }
  };
  const renderItemPhim = () => {
    return danhSachPhim.map((phim) => {
      return (
        <div className="flex w-full p-3" key={phim.maPhim}>
          <img src={phim.hinhAnh} className="w-32" alt="" />
          <div className="ml-5">
            <h1 className="text-left font-medium text-2xl">{phim.tenPhim}</h1>
            <h1 className="text-left font-medium">Đặt vé:</h1>
            <div className="grid grid-cols-3 gap-3 h-20 overflow-auto">
              {phim.lstLichChieuTheoPhim.map((lichChieu) => {
                return (
                  <div
                    onClick={() => {
                      const maLich = lichChieu.maLichChieu;
                      handleChonLichChieu(maLich);
                    }}
                    key={lichChieu.maLichChieu}
                    className="bg-green-400 text-slate-900 rounded p-1 h-8"
                  >
                    {moment(lichChieu.ngayChieuGioChieu).format(
                      "DD/MM/YY - HH:MM"
                    )}
                  </div>
                );
              })}
            </div>
          </div>
        </div>
      );
    });
  };
  return (
    <div className="h-screen overflow-scroll">
      {renderItemPhim()}
      <Modal
        title="Đăng Nhập"
        open={modalLogin}
        footer={null}
        afterClose={afterClose}
        onCancel={hideModalLogin}
      >
        <Form
          name="basic"
          labelCol={{ span: 8 }}
          wrapperCol={{ span: 16 }}
          initialValues={{ remember: true }}
          onFinish={onFinish}
          onFinishFailed={onFinishFailed}
          autoComplete="off"
        >
          <Form.Item
            label="Tài Khoản"
            name="taiKhoan"
            rules={[{ required: true, message: "Vui lòng điền tài khoản!" }]}
          >
            <Input />
          </Form.Item>

          <Form.Item
            label="Mật Khẩu"
            name="matKhau"
            rules={[{ required: true, message: "Vui lòng nhập mật khẩu!" }]}
          >
            <Input.Password />
          </Form.Item>

          <Form.Item wrapperCol={{ offset: 8, span: 16 }}>
            <Button type="primary" htmlType="submit" danger>
              Đăng Nhập
            </Button>
          </Form.Item>
          <p>
            Bạn chưa có tài khoản?{" "}
            <Button
              type="link"
              onClick={() => {
                setTimeout(() => {
                  navigate("/register");
                }, 1000);
              }}
              className="text-red-500 underline hover:text-green-400 p-0"
            >
              Đăng Ký
            </Button>
          </p>
        </Form>
      </Modal>
    </div>
  );
};

export default LichChieuPhim;
