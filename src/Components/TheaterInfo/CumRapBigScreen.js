import React from "react";
import { Tabs } from "antd";
import LichChieuPhim from "./LichChieuPhim";
import { Collapse } from "antd";
const { Panel } = Collapse;

const CumRapBigScreen = ({ listCumRap }) => {
  let items = [];
  listCumRap.map((cumRap) => {
    return items.push({
      label: (
        <div className="w-full overflow-auto">
          <div className="font-bold text-left">{cumRap.tenCumRap}</div>
          <div>Địa Chỉ: {cumRap.diaChi}</div>
        </div>
      ),
      key: cumRap.maCumRap,
      children: <LichChieuPhim danhSachPhim={cumRap.danhSachPhim} />,
    });
  });

  return (
    <Tabs
      tabPosition="left"
      items={items}
      className="h-screen overflow-scroll"
    />
  );
};

export default CumRapBigScreen;
