import React, { useEffect, useState } from "react";
import { Tabs } from "antd";
import { movieService } from "../../Services/movie.service";
import CumRapBigScreen from "./CumRapBigScreen";
import "./TheaterTab.css";
import { useMediaQuery } from "react-responsive";
import CumRapSmallScreen from "./CumRapSmallScreen";

const TheaterInfo = () => {
  const [theaterInfo, setTheaterInfo] = useState([]);
  const fetchTheaterInfo = async () => {
    try {
      let res = await movieService.getTheaterInfo();
      setTheaterInfo(res.data.content);
    } catch (err) {
      console.log(err);
    }
  };
  useEffect(() => {
    fetchTheaterInfo();
  }, []);

  // const renderHeThongRap = () => {
  //   return <Tabs />
  // }
  const isBigScreen = useMediaQuery({ query: "(min-width: 1024px)" });

  let items = [];

  theaterInfo.map((heThongRap) => {
    return items.push({
      label: <img src={heThongRap.logo} className="w-20" alt="theater-logo" />,
      key: `${heThongRap.maHeThongRap}`,
      children: isBigScreen ? (
        <CumRapBigScreen listCumRap={heThongRap.lstCumRap} />
      ) : (
        <CumRapSmallScreen listCumRap={heThongRap.lstCumRap} />
      ),
    });
  });
  return (
    <div className="container mx-auto bg-slate-50 lg:p-3 rounded">
      <Tabs
        tabPosition="top"
        items={items}
        animated
        centered
        tabBarGutter={30}
      />
    </div>
  );
};

export default TheaterInfo;
