import React, { useEffect, useState } from "react";
import { movieService } from "../../Services/movie.service";
import { Select } from "antd";
import { useNavigate } from "react-router-dom";
import { useMediaQuery } from "react-responsive";

export default function MovieSearchBar() {
  const navigate = useNavigate();
  const [movieList, setMovieList] = useState([]);
  const [options, setOptions] = useState([]);
  const isBigScreen = useMediaQuery({
    query: "(min-width: 1024px)",
  });
  const onChange = (value) => {
    console.log(`selected ${value}`);
    let index = movieList.findIndex((phim) => phim.tenPhim === value);
    let maPhim = movieList[index].maPhim;
    console.log(maPhim);
    navigate(`/detail/${maPhim}`);
  };

  const fetchMovieList = async (params) => {
    try {
      let res = await movieService.getMoviesList(params);
      let data = res.data.content.items;
      setMovieList(data);
      const list = [];
      data.forEach((phim) => {
        let item = {
          value: phim.tenPhim,
          label: phim.tenPhim,
        };
        list.push(item);
      });
      setOptions(list);
    } catch (err) {
      console.log(err);
    }
  };

  useEffect(() => {
    const params = {
      maNhom: "GP03",
      soTrang: 1,
      soPhanTuTrenTrang: 50,
    };
    fetchMovieList(params);
  }, []);
  // console.log(options);
  // console.log(movieList);
  return (
    <div className="w-full flex p-2 items-center justify-center">
      <div className="text-slate-900 font-medium bg-green-400 p-1 lg:p-2 rounded">
        Tìm Kiếm:
      </div>
      <Select
        showSearch
        style={{ width: "80%", padding: "5px" }}
        status="warning"
        size={isBigScreen ? "large" : "default"}
        placeholder="Nhập tên phim..."
        optionFilterProp="children"
        onChange={onChange}
        filterOption={(input, option) =>
          (option?.label ?? "").toLowerCase().includes(input.toLowerCase())
        }
        options={options}
      />
    </div>
  );
}
