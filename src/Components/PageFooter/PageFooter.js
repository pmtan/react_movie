import React from "react";
import "./PageFooter.css";

export default function PageFooter() {
  return (
    <footer className="footer bg-slate-50 w-full border-b-2 lg:mt-10">
      <div className="container w-full mx-auto">
        <div className="sm:flex sm:mt-8">
          <div className="mt-5 sm:mt-0 sm:w-full sm:px-8 sm:flex sm:flex-col lg:flex-row lg:justify-between">
            <div className="flex flex-col lg:w-2/5 sm: w-full sm:p-5">
              <span className="font-bold text-red-600 uppercase mb-2">
                MOVIE ZONE
              </span>
              <p className="text-slate-900 text-center lg:pt-1">
                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
                eiusmod tempor incididunt ut labore et dolore magna aliqua.
              </p>
            </div>
            <div className="hidden lg:flex lg:flex-col lg:text-left lg:w-1/6 lg:items-start">
              <div className="font-medium text-slate-900 uppercase">
                Company
              </div>
              <div className="w-4/5 mt-2 flex justify-around lg:flex lg:flex-col lg:pt-5">
                <span className="lg:my-1 text-slate-900 text-sm lg:text-md hover:text-red-500">
                  About
                </span>
                <span className="lg:my-1 text-slate-900 text-sm lg:text-md hover:text-red-500">
                  History
                </span>
                <span className="lg:my-1 text-slate-900 text-sm lg:text-md hover:text-red-500">
                  Careers
                </span>
              </div>
            </div>
            <div className="hidden lg:flex lg:flex-col lg:text-left lg:w-1/6 lg:items-start">
              <div className="font-medium text-slate-900 uppercase">
                Helpful Links
              </div>
              <div className="w-4/5 mt-2 flex justify-around lg:flex lg:flex-col lg:pt-5">
                <span className="lg:my-1 text-slate-900 text-sm lg:text-md hover:text-red-500">
                  FAQ
                </span>
                <span className="lg:my-1 text-slate-900 text-sm lg:text-md hover:text-red-500">
                  Live Chat
                </span>
                <span className="lg:my-1 text-slate-900 text-sm lg:text-md hover:text-red-500">
                  Terms & Conditions
                </span>
              </div>
            </div>
            <div className="flex flex-col items-center w-full mt-3 lg:text-left lg:w-1/6 lg:items-start">
              <span className="font-medium text-slate-900 uppercase">
                Contacts
              </span>
              <div className="flex flex-col w-4/5 mt-2 lg:pt-5 lg:w-full">
                <span className="lg:my-1 text-slate-900 text-sm lg:text-md hover:text-red-500">
                  Address: 123 Movie Street, HCMC, Vietnam
                </span>
                <span className="lg:my-1 text-slate-900 text-sm lg:text-md hover:text-red-500">
                  Email: service@moviezone.com
                </span>
                <span className="lg:my-1 text-slate-900 text-sm lg:text-md hover:text-red-500">
                  Phone: (012)-345-6789
                </span>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div className="container mx-auto px-6">
        <div className="mt-5 border-t border-slate-900 flex flex-col items-center">
          <div className="sm:w-2/3 text-center py-2">
            <p className="text-sm text-slate-900 font-bold mb-2">
              © 2022 by Movie Zone
            </p>
          </div>
        </div>
      </div>
    </footer>
  );
}
