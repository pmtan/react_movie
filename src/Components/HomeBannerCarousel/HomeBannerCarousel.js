import React, { useEffect, useState } from "react";
import { movieService } from "../../Services/movie.service";
import Slider from "react-slick";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";

const HomeBannerCarousel = () => {
  const settings = {
    infinite: true,
    speed: 500,
    slidesToShow: 1,
    slidesToScroll: 1,
    // autoplay: true,
    autoplaySpeed: 3000,
    centerMOde: true,
  };
  const [banner, setBanner] = useState([]);
  const fetchBanner = async () => {
    try {
      let res = await movieService.getBanner();
      setBanner(res.data.content);
    } catch (err) {
      console.log(err);
    }
  };
  useEffect(() => {
    fetchBanner();
  }, []);

  return (
    <Slider {...settings} className="w-full">
      {banner.map((item) => {
        return (
          <div key={item.maBanner}>
            <img src={item.hinhAnh} className="w-full h-full bg-cover" alt="" />
          </div>
        );
      })}
    </Slider>
  );
};

export default HomeBannerCarousel;
