import React, { useEffect, useState } from "react";
import { movieService } from "../../Services/movie.service";
import { useNavigate } from "react-router-dom";
import Slider from "react-slick";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";

const MovieGrid = () => {
  let navigate = useNavigate();
  const settings = {
    infinite: true,
    speed: 500,
    slidesToShow: 3,
    slidesToScroll: 3,
    autoplay: true,
    autoplaySpeed: 8000,
    easing: "ease-out",
  };
  const [movieList, setMovieList] = useState([]);
  const fetchMovieList = async (params) => {
    try {
      let res = await movieService.getMoviesList(params);
      // console.log(res.data.content);
      setMovieList(res.data.content.items);
    } catch (err) {
      console.log(err);
    }
  };
  useEffect(() => {
    const params = {
      maNhom: "GP03",
      soTrang: 1,
      soPhanTuTrenTrang: 50,
    };
    fetchMovieList(params);
  }, []);
  const renderMovieList = () => {
    return movieList.map((movie) => {
      return (
        <div
          onClick={() => {
            navigate(`detail/${movie.maPhim}`);
          }}
          key={movie.maPhim}
          className="p-5"
        >
          <img
            src={movie.hinhAnh}
            className="w-full h-full rounded"
            alt={movie.tenPhim}
          />
        </div>
      );
    });
  };

  return (
    // <div className="container grid grid-cols-4 mx-auto gap-10 py-10">
    <div className="container mx-auto">
      <Slider {...settings}>{renderMovieList()}</Slider>
    </div>
  );
};

export default MovieGrid;
