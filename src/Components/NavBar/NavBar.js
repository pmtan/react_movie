import React, { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { NavLink, useNavigate } from "react-router-dom";
import { setUserInfoStore } from "../../Redux/UserSlice";
import { localService } from "../../Services/local.service";

const NavBar = () => {
  const [navbar, setNavbar] = useState(false);
  let dispatch = useDispatch();
  let navigate = useNavigate();
  const handleLogOut = () => {
    localService.remove();
    dispatch(setUserInfoStore(null));
    navigate("/");
  };

  let userInfo = localService.get();
  const renderLoginUser = () => {
    if (userInfo) {
      return (
        <div>
          <span className="text-red-500 font-medium text-lg">
            {userInfo.hoTen}
          </span>
          <button
            onClick={handleLogOut}
            className="bg-red-600 text-white p-2 lg:p-3 lg:mx-3 rounded shadow-lg"
          >
            Đăng Xuất
          </button>
        </div>
      );
    } else {
      return (
        <div>
          <NavLink
            to="/login"
            className="bg-red-600 text-white p-1 lg:p-2 mx-3 rounded shadow-lg"
          >
            Đăng Nhập
          </NavLink>
          <NavLink
            to="/register"
            className="bg-black text-white p-1 lg:p-2 mx-3 rounded shadow-lg"
          >
            Đăng Ký
          </NavLink>
        </div>
      );
    }
  };

  return (
    <nav className="w-full bg-slate-50 sticky z-10 shadow">
      <div className="justify-between px-4 mx-auto lg:max-w-7xl md:items-center md:flex md:px-8">
        <div>
          <div className="flex items-center justify-between py-1 md:py-5 md:block">
            <span
              onClick={() => {
                navigate("/");
              }}
              className="logo text-xl font-bold m-3 text-red-500"
            >
              MOVIE ZONE
            </span>
            <div className="md:hidden">
              <button
                className="p-2 text-slate-900 rounded-md outline-none focus:border-red-500 focus:text-red-500"
                onClick={() => setNavbar(!navbar)}
              >
                {navbar ? (
                  <svg
                    xmlns="http://www.w3.org/2000/svg"
                    className="w-6 h-6"
                    viewBox="0 0 20 20"
                    fill="currentColor"
                  >
                    <path
                      fillRule="evenodd"
                      d="M4.293 4.293a1 1 0 011.414 0L10 8.586l4.293-4.293a1 1 0 111.414 1.414L11.414 10l4.293 4.293a1 1 0 01-1.414 1.414L10 11.414l-4.293 4.293a1 1 0 01-1.414-1.414L8.586 10 4.293 5.707a1 1 0 010-1.414z"
                      clipRule="evenodd"
                    />
                  </svg>
                ) : (
                  <svg
                    xmlns="http://www.w3.org/2000/svg"
                    className="w-6 h-6"
                    fill="none"
                    viewBox="0 0 24 24"
                    stroke="currentColor"
                    strokeWidth={2}
                  >
                    <path
                      strokeLinecap="round"
                      strokeLinejoin="round"
                      d="M4 6h16M4 12h16M4 18h16"
                    />
                  </svg>
                )}
              </button>
            </div>
          </div>
        </div>
        <div>
          <div
            className={`flex-1 justify-self-center pb-3 mt-3 md:block md:pb-0 md:mt-0 ${
              navbar ? "block" : "hidden"
            }`}
          >
            <ul className="items-center justify-center space-y-4 md:flex md:space-x-6 md:space-y-0">
              <li
                onClick={() => {
                  navigate("/");
                }}
                className="text-slate-900 focus:text-red-500"
              >
                <span>Trang Chủ</span>
              </li>
              <li
                onClick={() => {
                  navigate("/user-info");
                }}
                className="text-slate-900 focus:text-red-500"
              >
                <span>Thông Tin Cá Nhân</span>
              </li>
              <li className="text-slate-900 focus:text-red-500">
                <span>Tin Tức</span>
              </li>
              <li className="text-slate-900 focus:text-red-500">
                <span>Liên Hệ</span>
              </li>
              <li>{renderLoginUser()}</li>
            </ul>
          </div>
        </div>
      </div>
    </nav>
  );
};

export default NavBar;
