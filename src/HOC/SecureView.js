import { useEffect } from "react";
import { localService } from "../Services/local.service";

export default function SecureView({ Component }) {
  useEffect(() => {
    let user = localService.get();
    if (user == null) {
      window.location.href = "/login";
      localService.remove();
    }
  }, []);
  return <Component />;
}
